package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CheckInModel {
    public int LikesNum,ID;
    public String text,userName;

    public CheckInModel(int ID, String text, String userName) {
        this.ID = ID;
        this.text = text;
        this.userName = userName;
        LikesNum=0;
    }

    public CheckInModel() {
    }
    
    
    
    
    public static Connection GetConnection(){
		try{			
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/software","root"
					,"");
			System.out.println("Connected");
			return conn;
			
		}catch (Exception e){  System.out.println(e); }
    return null;
    }       
    public void saveCheckIn() throws SQLException{
        
        Connection con=GetConnection();
        PreparedStatement statement2=con.prepareStatement("INSERT INTO `checkin`(`LikesIn`, `CheckInText`, `UserName`, `PlaceID`) VALUES (0,'"+text+"','"+userName+"',"+ID+")");                        
        statement2.executeUpdate();
    }
    static public ArrayList <CheckInModel> getCheckIns() throws SQLException{
        Connection con=GetConnection();
        PreparedStatement statement2=con.prepareStatement("Select * from `checkin`");                        
        ResultSet result = statement2.executeQuery();
        ArrayList<CheckInModel>cm=new ArrayList<CheckInModel>();
        CheckInModel m=null;
        while (result.next()){
            m=new CheckInModel();
            m.LikesNum=result.getInt("LikesIn");
            m.ID=result.getInt("PlaceID");
            m.text=result.getString("CheckInText");
            m.userName=result.getString("UserName");            
            cm.add(m);            
        }
        return cm;
    }
}
