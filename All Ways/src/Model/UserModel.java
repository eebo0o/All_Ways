package Model;

import java.sql.*;
import java.util.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserModel {
    public String UserName,Password,ConfirmPassword,User_Type,Email,Phone_Number,Address;
    public String Gender;
    public ArrayList<PlaceModel> Place;
    //TasteModel Tastes; 

    public UserModel(String UserName, String Password,String ConfirmPassword , String Email, String Phone_Number,String Address, String Gender) {
        this.UserName = UserName;
        this.Password = Password;
        this.ConfirmPassword=ConfirmPassword;
//        this.User_Type = User_Type;
        this.Email = Email;
        this.Phone_Number = Phone_Number;
        this.Gender = Gender;
        this.Address =Address;
    }
     public UserModel(UserModel user)
     {
         UserName=user.UserName;
         ConfirmPassword=user.ConfirmPassword;
         Password=user.Password;
         User_Type=user.User_Type;
         Phone_Number=user.Phone_Number;
         Email=user.Email;
         Gender=user.Gender;
         Address=user.Address;
     }
    public UserModel() {
        UserName = "";
        this.Password = "";
        this.ConfirmPassword="";
       this.User_Type = User_Type;
        this.Email = "";
        this.Phone_Number = "";
        this.Gender = "";
        this.Address ="";
    }
    

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }
    public void setPassword(String Password) {
        this.Password = Password;
    }
    public void setUser_Type(String User_Type) {
        this.User_Type = User_Type;
    }
    public void setEmail(String Email) {
        this.Email = Email;
    }
    public void setPhone_Number(String Phone_Number) {
        this.Phone_Number = Phone_Number;
    }
    public void setGender(String Gender) {
        this.Gender = Gender;
    }
    public void setAddress(String a){
        Address = a;
    }
    public String getUserName() {
        return UserName;
    }
    public String getPassword() {
        return Password;
    }
    public String getType() {
        return User_Type;
    }
    public String getEmail() {
        return Email;
    }
    public String getPhone_Number() {
        return Phone_Number;
    }
    public String getGender() {
        return Gender;
    }
    public String getAddress(){
        return Address;
    }
    public String getUser_Type(){
        return User_Type;
    }
    
    public static Connection GetConnection(){
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/software","root","");
			System.out.println("Connected");
			return conn;
			
		}catch (Exception e){  System.out.println(e); }
    return null;
    }
    
    static public void addNewUser(UserModel U){
        try{
            Connection conn=GetConnection();
            PreparedStatement posted=conn.prepareStatement("INSERT INTO user(UserName,Password,email,PhoneNum,Address,type,Gender) VALUES('"+U.getUserName()+"','"+U.getPassword()+"','"+U.getEmail()+"','"+U.getPhone_Number()+"','"+U.getAddress()+"','"+U.getUser_Type()+"','"+U.getGender()+"')");
            posted.executeUpdate();
        }catch (Exception e){  System.out.println(e); }
        
    }
    public static boolean CheckValidation(String username) throws SQLException{
        Connection con=GetConnection();
        PreparedStatement statement=con.prepareStatement("SELECT * from user WHERE UserName = '"+username+"'");
        ResultSet result=statement.executeQuery();
        if(result.next())
            return false;
        else
            return true;
    }
    static public UserModel getUser(String name , String Password) throws SQLException{
        Connection con=GetConnection();
        PreparedStatement statement=con.prepareStatement("SELECT * from user where UserName = '"+name+"' and Password = '"+Password+"'");
        ResultSet result=statement.executeQuery();
        UserModel U=null;
        int ID = 0;
        while(result.next()){
            U=new UserModel();
            U.UserName = result.getString("UserName");
            U.Password = result.getString("Password");
            U.Email = result.getString("email");
            U.Phone_Number = result.getString("PhoneNum");
            U.User_Type = result.getString("type");
            U.Gender = result.getString("Gender");
            U.Address = result.getString("Address");
        PreparedStatement statement2=con.prepareStatement("SELECT * FROM `savedplaces` WHERE UserName = '"+name+"'");
        ResultSet result2=statement2.executeQuery();
        ArrayList<PlaceModel>P=new ArrayList <PlaceModel>();
        PlaceModel pp=null;                
        while(result2.next()){
            pp=new PlaceModel();
            pp.Place_ID=result2.getInt("PlaceID");
            
            PreparedStatement statement3=con.prepareStatement("SELECT * FROM `places` WHERE  ID = "+pp.Place_ID);
            ResultSet result3=statement3.executeQuery();
            if(result3.next()){
                pp.Place_Name=result3.getString("Name");
                pp.Rate=Float.parseFloat(result3.getString("Rate"));
                pp.CheckInSum=result3.getInt("fCheckInsNum");
                pp.SavedNum=result3.getInt("SavedNum");
                pp.Place_Description=result3.getString("description");               
                P.add(pp);
            }            
        }
        U.Place = P;
        }
        
        
        
        return U;
    }
    public void deleteUser(String n) throws SQLException{
        Connection con=GetConnection();       
        PreparedStatement statement=con.prepareStatement("DELETE FROM `friends` WHERE (Accept = "+n+"'and Request'"+this.UserName+"') or ( Request = "+n+"'and Accept'"+this.UserName+"')" );
        statement.executeUpdate();
    }
    public void updateUser(){
        
    }
    public void addNewFreind(String n) throws SQLException{
        Connection con=GetConnection();       
        PreparedStatement statement=con.prepareStatement("INSERT INTO `friends`(`Request`, `Accept`) VALUES (' "+n+"','"+this.UserName+"')" );
        statement.executeUpdate();
       
    }
    public ArrayList<UserModel> getFriends() throws SQLException{
         Connection con=GetConnection();
        PreparedStatement statement=con.prepareStatement("SELECT * from friends where `Accept` ='"+UserName+"'or Request = '"+this.UserName+"'");
        ResultSet result=statement.executeQuery();
        ArrayList <UserModel> F=new ArrayList<UserModel>();
        UserModel u=null;
        String freind="";
        while(result.next()){            
            freind=result.getString("Accept");
            if(freind !=UserName){
            PreparedStatement statement2=con.prepareStatement("SELECT * from user where UserName ='"+freind+"'");
            ResultSet result2=statement2.executeQuery();
            UserModel U = null;
        if(result.next()){
            U =new UserModel();
            U.UserName = result.getString("UserName");
            U.Password = result.getString("Password");
            U.Email = result.getString("email");
            U.Phone_Number = result.getString("PhoneNum");
            U.User_Type = result.getString("type");
            U.Gender = result.getString("Gender");
            U.Address = result.getString("Address");            
            F.add(U);
            }
            
        }
            else{
                PreparedStatement statement2=con.prepareStatement("SELECT * from user where UserName ='"+result.getString("Request")+"'");
            ResultSet result2=statement2.executeQuery();
            UserModel U = null;
        if(result.next()){
            U =new UserModel();
            U.UserName = result.getString("UserName");
            U.Password = result.getString("Password");
            U.Email = result.getString("email");
            U.Phone_Number = result.getString("PhoneNum");
            U.User_Type = result.getString("type");
            U.Gender = result.getString("Gender");
            U.Address = result.getString("Address");            
            F.add(U);
                }
            }       
    }
        return F;
    }
     public ArrayList<UserModel> NotFriends() throws SQLException{
        
         Connection con=GetConnection();
        PreparedStatement statement=con.prepareStatement("SELECT * from user");
        ResultSet result=statement.executeQuery();
        ArrayList <UserModel> F=new ArrayList<UserModel>();
        while(result.next()){           
            UserModel U = null;
            U =new UserModel();
            U.UserName = result.getString("UserName");
            U.Password = result.getString("Password");
            U.Email = result.getString("email");
            U.Phone_Number = result.getString("PhoneNum");
            U.User_Type = result.getString("type");
            U.Gender = result.getString("Gender");
            U.Address = result.getString("Address"); 
            PreparedStatement statement2=con.prepareStatement("SELECT * FROM `friends` WHERE (Accept ='"+UserName+"' and Request ='"+U.UserName+"') or (Accept ='"+U.UserName+"' and Request ='"+UserName+"')");
            ResultSet result2=statement2.executeQuery();           
            if(! result2.next())
                F.add(U);
            
        }
        return F;
    }
    static public UserModel getUserByPassword(String name,String Emails,String Phones) throws SQLException{
         Connection con=GetConnection();
        PreparedStatement statement=con.prepareStatement("SELECT * from user where UserName ='"+name+"'and email = '"+Emails+"' and PhoneNum = '"+Phones+"'");
        ResultSet result=statement.executeQuery();
        UserModel U = null;
        if(result.next()){
            U =new UserModel();
            U.UserName = result.getString("UserName");
            U.Password = result.getString("Password");
            U.Email = result.getString("email");
            U.Phone_Number = result.getString("PhoneNum");
            U.User_Type = result.getString("type");
            U.Gender = result.getString("Gender");
            U.Address = result.getString("Address");
            PreparedStatement statement2=con.prepareStatement("SELECT * FROM `savedplaces` WHERE UserName = '"+name+"'");
            ResultSet result2=statement2.executeQuery();
            ArrayList<PlaceModel>P=new ArrayList <PlaceModel>();
            PlaceModel pp=null;                
            while(result2.next()){
            pp=new PlaceModel();
            pp.Place_ID=result2.getInt("PlaceID");
            
            PreparedStatement statement3=con.prepareStatement("SELECT * FROM `places` WHERE  ID = "+pp.Place_ID);
            ResultSet result3=statement3.executeQuery();
            if(result3.next()){
                pp.Place_Name=result3.getString("Name");
                pp.Rate=Float.parseFloat(result3.getString("Rate"));
                pp.CheckInSum=result3.getInt("fCheckInsNum");
                pp.SavedNum=result3.getInt("SavedNum");
                pp.Place_Description=result3.getString("description");               
                P.add(pp);
            }            
        }
        U.Place = P;
        }
        return U;
    }
    
    
    
}
