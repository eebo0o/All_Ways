package Model;

import static Model.UserModel.GetConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PlaceModel {
    public int Place_ID,CheckInSum;
    public String Place_Name,Place_Description;
    public double Rate;
    public int SavedNum=0;
    
    public static Connection GetConnection(){
		try{			
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/software","root"
					,"");
			System.out.println("Connected");
			return conn;
			
		}catch (Exception e){  System.out.println(e); }
    return null;
    }    
    
    public void SavePlace(String name){
        PreparedStatement statement;
        ResultSet result;
        try{            
            Connection con=GetConnection();
            //Increase Saved Places
            
            statement=con.prepareStatement("SELECT `SavedNum` FROM `places` WHERE ID = "+Place_ID);
            result=statement.executeQuery();
            System.out.println("DDDD");
            if(result.next()){
                int num=result.getInt("SavedNum");
                num++;
                PreparedStatement statement3=con.prepareStatement("INSERT INTO `savedplaces`(`UserName`, `PlaceID`) VALUES ('"+name+"',"+Place_ID+")");
                statement3.executeUpdate();
                PreparedStatement statement2=con.prepareStatement("UPDATE `places` SET `SavedNum` ="+num+" WHERE ID ="+Place_ID);                        
                statement2.executeUpdate();
            }
        }catch (Exception e){  System.out.println(e); }
        
    }       
    static public PlaceModel getPlace(int ID)throws SQLException{
        Connection con=GetConnection();
        PreparedStatement statement=con.prepareStatement("SELECT * from places where ID = "+ID);
        ResultSet result=statement.executeQuery();
        PlaceModel P=new PlaceModel();
        while(result.next()){
            P.Place_ID=result.getInt("ID");
            P.Place_Name=result.getString("Name");
            P.Rate=result.getDouble("Rate");
            P.CheckInSum=result.getInt("fCheckInsNum");
            P.SavedNum=result.getInt("SavedNum");
            P.Place_Description=result.getString("description");        
        }
        return P;
    }
    public void deletePlace() throws SQLException{
        Connection con=GetConnection();
        PreparedStatement statement=con.prepareStatement("DELETE FROM `places` WHERE ID = "+Place_ID);
        statement.executeQuery();
    }
    public void updatePlace(){
        
    }
    
    public static ArrayList< PlaceModel> allPlaces() throws SQLException{
        ArrayList<PlaceModel>PP=new ArrayList<PlaceModel>();
        Connection con=GetConnection();
        PreparedStatement statement2=con.prepareStatement("SELECT * from places");
        ResultSet result2=statement2.executeQuery();
        PlaceModel P=null;
        while(result2.next()){
            P=new PlaceModel();
            P.Place_ID=result2.getInt("ID");
            P.Place_Name=result2.getString("Name");
            P.Rate=result2.getDouble("Rate");
            P.CheckInSum=result2.getInt("fCheckInsNum");
            P.SavedNum=result2.getInt("SavedNum");
            P.Place_Description=result2.getString("description");        
            PP.add(P);
        }        
        return PP;        
    }
    
}
