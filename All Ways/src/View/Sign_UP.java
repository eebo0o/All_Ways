package View;
import Controller.*;
import Model.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Sign_UP extends javax.swing.JFrame {

        private UserModel U=null;
        private boolean check =true,check2=true;
        
    public Sign_UP() {
        initComponents();
       Male.setBackground(null);
       Female.setBackground(null);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel10 = new javax.swing.JLabel();
        jLabelEmail = new javax.swing.JLabel();
        jLabelPhoneNumber = new javax.swing.JLabel();
        jLabelPassword = new javax.swing.JLabel();
        jLabelConfirmPassword = new javax.swing.JLabel();
        jLabelGender = new javax.swing.JLabel();
        jLabelUserName = new javax.swing.JLabel();
        jLabelAddress = new javax.swing.JLabel();
        Email = new javax.swing.JTextField();
        PhoneNumber = new javax.swing.JTextField();
        Address = new javax.swing.JTextField();
        UserName = new javax.swing.JTextField();
        Female = new javax.swing.JRadioButton();
        Male = new javax.swing.JRadioButton();
        Submit = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        ConfirmPaswword = new javax.swing.JPasswordField();
        Password = new javax.swing.JPasswordField();
        Valid2 = new javax.swing.JLabel();
        Invalid2 = new javax.swing.JLabel();
        Valid1 = new javax.swing.JLabel();
        Invalid1 = new javax.swing.JLabel();
        Invalid3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        jLabel10.setBackground(new java.awt.Color(255, 102, 0));
        jLabel10.setForeground(new java.awt.Color(255, 102, 0));
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/gps_location_map_marker-5125.png"))); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("All Ways Sign Up");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelEmail.setText("Email:");
        getContentPane().add(jLabelEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, -1, -1));

        jLabelPhoneNumber.setText("Phone Number:");
        getContentPane().add(jLabelPhoneNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 200, -1, -1));

        jLabelPassword.setText("Password:");
        getContentPane().add(jLabelPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, -1, -1));

        jLabelConfirmPassword.setText("Confirm Password:");
        getContentPane().add(jLabelConfirmPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        jLabelGender.setText("Gender:");
        getContentPane().add(jLabelGender, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 280, -1, -1));

        jLabelUserName.setText("User Name: ");
        getContentPane().add(jLabelUserName, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, -1, -1));

        jLabelAddress.setText("Address:");
        getContentPane().add(jLabelAddress, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 240, -1, -1));
        getContentPane().add(Email, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 150, 210, -1));
        getContentPane().add(PhoneNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 190, 210, -1));
        getContentPane().add(Address, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 230, 210, -1));

        UserName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                UserNameFocusLost(evt);
            }
        });
        getContentPane().add(UserName, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 30, 210, -1));

        Female.setText("Female");
        Female.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FemaleMouseClicked(evt);
            }
        });
        Female.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FemaleActionPerformed(evt);
            }
        });
        getContentPane().add(Female, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 280, -1, -1));

        Male.setText("Male");
        Male.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MaleActionPerformed(evt);
            }
        });
        getContentPane().add(Male, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 280, -1, -1));

        Submit.setBackground(new java.awt.Color(255, 153, 0));
        Submit.setText("Submmit");
        Submit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubmitActionPerformed(evt);
            }
        });
        getContentPane().add(Submit, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 320, 120, -1));

        jLabel11.setBackground(new java.awt.Color(255, 102, 0));
        jLabel11.setForeground(new java.awt.Color(255, 102, 0));
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/gps_location_map_marker-5125.png"))); // NOI18N
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 290, 60, 90));

        ConfirmPaswword.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                ConfirmPaswwordFocusLost(evt);
            }
        });
        getContentPane().add(ConfirmPaswword, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 90, 210, -1));

        Password.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PasswordActionPerformed(evt);
            }
        });
        getContentPane().add(Password, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 60, 210, -1));

        Valid2.setVisible(false);
        Valid2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/valid.png"))); // NOI18N
        getContentPane().add(Valid2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 90, -1, 20));

        Invalid2.setVisible(false);
        getContentPane().add(Invalid2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 90, -1, -1));

        Valid1.setVisible(false);
        Valid1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/valid.png"))); // NOI18N
        getContentPane().add(Valid1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 30, -1, -1));

        Invalid1.setVisible(false);
        Invalid1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Invalid.png"))); // NOI18N
        getContentPane().add(Invalid1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 30, -1, -1));

        Invalid3.setVisible(false);
        Invalid3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Invalid.png"))); // NOI18N
        getContentPane().add(Invalid3, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 90, -1, -1));

        jLabel9.setBackground(new java.awt.Color(255, 204, 102));
        jLabel9.setForeground(new java.awt.Color(255, 102, 0));
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/gps_location_map_marker-51299992.png"))); // NOI18N
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 420, 360));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/gps_location_map_marker-5127.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 500, 380));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void SubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubmitActionPerformed
       
        if (check&&check2)
        {
            try {
                if(Male.getText().equals("")){
                    U=new UserModel(UserName.getText(),Password.getText(),ConfirmPaswword.getText(),Email.getText(),PhoneNumber.getText(),Address.getText(),Female.getText());
                }
                else{
                    U=new UserModel(UserName.getText(),Password.getText(),ConfirmPaswword.getText(),Email.getText(),PhoneNumber.getText(),Address.getText(),Male.getText());
                }
                UserController User = new UserController();
                User.setUser(U);
                UserController.signUp(U);
                Main_Menu comperation = new Main_Menu(User,PlaceModel.allPlaces());
                comperation.setVisible(true);
                this.dispose();
                JOptionPane.showMessageDialog(null, "Succefful Registring.");
            } catch (SQLException ex) {
                Logger.getLogger(Sign_UP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else 
        {
            JOptionPane.showMessageDialog(null, "Error....User name is Invalid.");
        }
            
    }//GEN-LAST:event_SubmitActionPerformed

    private void PasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PasswordActionPerformed

    private void MaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MaleActionPerformed

            Male.setSelected(true);
            Female.setSelected(false);
    }//GEN-LAST:event_MaleActionPerformed

    private void FemaleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FemaleMouseClicked
        
            Male.setSelected(false);
            Female.setSelected(true);
    }//GEN-LAST:event_FemaleMouseClicked

    private void FemaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FemaleActionPerformed
            Male.setSelected(false);
            Female.setSelected(true);
    }//GEN-LAST:event_FemaleActionPerformed

    private void UserNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_UserNameFocusLost
            try {
                if (UserModel.CheckValidation(UserName.getText()))
                {
                    Valid1.setVisible(true);
                    Invalid1.setVisible(false);
                    check=true;
                }else
                {
                    JOptionPane.showMessageDialog(null, "User name is Invalid.");
                    Valid1.setVisible(false);
                    Invalid1.setVisible(true);
                    check=false;
                }    
            } catch (SQLException ex) {
                Logger.getLogger(Sign_UP.class.getName()).log(Level.SEVERE, null, ex);
            }
       
    }//GEN-LAST:event_UserNameFocusLost

    private void ConfirmPaswwordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ConfirmPaswwordFocusLost
       if (Password.getText().equals(ConfirmPaswword.getText()))
       {
           Valid2.setVisible(true);
           Invalid3.setVisible(false);
           check2=true;
       }else 
       {
           JOptionPane.showMessageDialog(null, "Passwords Dosen't matched.");
           Valid2.setVisible(false);
           Invalid3.setVisible(true);
           check2=false;
       }
    }//GEN-LAST:event_ConfirmPaswwordFocusLost

    
    public void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Sign_UP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Sign_UP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Sign_UP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Sign_UP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Sign_UP().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Address;
    private javax.swing.JPasswordField ConfirmPaswword;
    private javax.swing.JTextField Email;
    private javax.swing.JRadioButton Female;
    private javax.swing.JLabel Invalid1;
    private javax.swing.JLabel Invalid2;
    private javax.swing.JLabel Invalid3;
    private javax.swing.JRadioButton Male;
    private javax.swing.JPasswordField Password;
    private javax.swing.JTextField PhoneNumber;
    private javax.swing.JButton Submit;
    private javax.swing.JTextField UserName;
    private javax.swing.JLabel Valid1;
    private javax.swing.JLabel Valid2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelAddress;
    private javax.swing.JLabel jLabelConfirmPassword;
    private javax.swing.JLabel jLabelEmail;
    private javax.swing.JLabel jLabelGender;
    private javax.swing.JLabel jLabelPassword;
    private javax.swing.JLabel jLabelPhoneNumber;
    private javax.swing.JLabel jLabelUserName;
    // End of variables declaration//GEN-END:variables
}
