/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package View;
import Controller.*;
import Model.CheckInModel;
import Model.PlaceModel;
import Model.UserModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 *
 * @author almos
 */
public class Main_Menu extends javax.swing.JFrame {
    ArrayList <CheckInModel> cm=new ArrayList <CheckInModel>();
    ArrayList <UserModel> Friend = new ArrayList<UserModel>();
    ArrayList <UserModel> newFriend = new ArrayList<UserModel>();
   ArrayList <PlaceModel> PP=new ArrayList <PlaceModel>(); 
   UserController User ;
   
   
    private ArrayList<javax.swing.JScrollPane> jScrollPane8;
    private ArrayList<javax.swing.JPanel> jPanel25;
    private ArrayList<javax.swing.JLabel> jLabel38;
    private ArrayList<javax.swing.JLabel> jLabel39;
    private ArrayList<javax.swing.JButton> JButtonx;
    private ArrayList<javax.swing.JButton> JButtony;
    private ArrayList<javax.swing.JTextArea> jTextArea4;
    boolean visablity1=false,visablity2=false;
    public Main_Menu(UserController user,ArrayList <PlaceModel> P) {
        try {
            initComponents();
            PP=P;
            User = user;
            cm = CheckInModel.getCheckIns();
            Friend = User.getUser().getFriends();
            newFriend = User.getUser().NotFriends();
            Show();
            Show_Checks_in();
            Show_Live_Notfications(10);
            Show_Friends_Request(10);
            if(Friend.size() > 0){
                String arr3[]=new String[Friend.size()];
                for(int i=0;i<arr3.length;i++)arr3[i]=Friend.get(i).UserName ;
                FrinList.setListData(arr3);
                
            }
            else{
                FrinList.setEnabled(false);
            }
            if(newFriend.size() > 0){
                String arr3[]=new String[newFriend.size()];
                for(int i=0;i<arr3.length;i++)arr3[i]=newFriend.get(i).UserName ;
                FrinList2.setListData(arr3);
                
            }
            else{
                FrinList2.setEnabled(false);
            }
            if(PP.size() > 0){
                String arr[]=new String[PP.size()];
                for(int i=0;i<arr.length;i++)arr[i]=PP.get(i).Place_Name;
                AllList.setListData(arr);
                
            }
            else{
                AllList.setEnabled(false);
            }
            
            if(User.getUser().Place.size() > 0){
                String arr2[] = new String [User.getUser().Place.size()];
                for(int i=0;i<arr2.length;i++)arr2[i]=User.getUser().Place.get(i).Place_Name;
                FavList.setListData(arr2);
            }
            else{
                FavList.setEnabled(false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main_Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    
    
       
    public void Show_Live_Notfications(int size)
   {
        jScrollPane8=new ArrayList<javax.swing.JScrollPane>();
        jPanel25=new ArrayList<javax.swing.JPanel>();
        jLabel38=new ArrayList<javax.swing.JLabel>();
        JButtonx=new ArrayList<javax.swing.JButton>();
        jTextArea4=new ArrayList<javax.swing.JTextArea>();
        
        
        for (int i =0; i<size;i++)
        {
            javax.swing.JScrollPane jScrollPane9= new JScrollPane();
            javax.swing.JPanel jPanel26= new JPanel();
            javax.swing.JLabel jLabel40=new JLabel();
            javax.swing.JButton Button=new JButton();
            javax.swing.JTextArea jTextArea5=new JTextArea();
            jScrollPane8.add(jScrollPane9);
            jPanel25.add(jPanel26);
            jLabel38.add(jLabel40);
            JButtonx.add(Button);
            jTextArea4.add(jTextArea5);
        jPanel25.get(i).setBackground(new java.awt.Color(255, 255, 255));
        jPanel25.get(i).setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 0), 2));
        jPanel25.get(i).setMinimumSize(new java.awt.Dimension(422, 108));
        jPanel25.get(i).setPreferredSize(new java.awt.Dimension(490, 102));
        jPanel25.get(i).setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel38.get(i).setText("Check in title :");
        jPanel25.get(i).add(jLabel38.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 200, -1));

        jTextArea4.get(i).setColumns(20);
        jTextArea4.get(i).setRows(5);
        jScrollPane8.get(i).setViewportView(jTextArea4.get(i));

        jPanel25.get(i).add(jScrollPane8.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 360, 69));

        JButtonx.get(i).setBackground(new java.awt.Color(255, 255, 255));
        JButtonx.get(i).setForeground(new java.awt.Color(255, 255, 255));
        JButtonx.get(i).setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Like-Button.png"))); // NOI18N
        JButtonx.get(i).addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonX_Live_ActionPerformed(evt);
            }
        });
        jPanel25.get(i).add(JButtonx.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 40, 50, 50));

        jPanel15.add(jPanel25.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(0, i*125, 490, 120));
        }
   }
   public void Show_Friends_Request(int size)
   {
        jScrollPane8=new ArrayList<javax.swing.JScrollPane>();
        jPanel25=new ArrayList<javax.swing.JPanel>();
        jLabel38=new ArrayList<javax.swing.JLabel>();
        JButtonx=new ArrayList<javax.swing.JButton>();
        JButtony=new ArrayList<javax.swing.JButton>();
        jTextArea4=new ArrayList<javax.swing.JTextArea>();
        
        
        for (int i =0; i<size;i++)
        {
            javax.swing.JScrollPane jScrollPane9= new JScrollPane();
            javax.swing.JPanel jPanel26= new JPanel();
            javax.swing.JLabel jLabel40=new JLabel();
            javax.swing.JButton Button=new JButton();
            javax.swing.JButton Button2=new JButton();
            jScrollPane8.add(jScrollPane9);
            jPanel25.add(jPanel26);
            jLabel38.add(jLabel40);
            JButtonx.add(Button);
            JButtony.add(Button2);
        jPanel25.get(i).setBackground(new java.awt.Color(255, 255, 255));
        jPanel25.get(i).setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 0), 2));
        jPanel25.get(i).setMinimumSize(new java.awt.Dimension(422, 108));
        jPanel25.get(i).setPreferredSize(new java.awt.Dimension(490, 102));
        jPanel25.get(i).setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel38.get(i).setText("Check in title :");
        jPanel25.get(i).add(jLabel38.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 200, -1));

       
        JButtony.get(i).setBackground(new java.awt.Color(255, 255, 255));
        JButtony.get(i).setText("Discard");
        JButtony.get(i).addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonY_Friend_ActionPerformed(evt);
            }
        });
        jPanel25.get(i).add(JButtony.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 30, 100, 30));
        JButtonx.get(i).setBackground(new java.awt.Color(255, 255, 255));
        JButtonx.get(i).setText("Accept");
        JButtonx.get(i).addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonX_Friend_ActionPerformed(evt);
            }
        });
        jPanel25.get(i).add(JButtonx.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 30, 90, 30));

        jPanel16.add(jPanel25.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(0, i*85, 490, 80));
        }
   }
   
    
    
    
       public void Show_Checks_in() throws SQLException
   {
        jScrollPane8=new ArrayList<javax.swing.JScrollPane>();
        jPanel25=new ArrayList<javax.swing.JPanel>();
        jLabel38=new ArrayList<javax.swing.JLabel>();
        jLabel39=new ArrayList<javax.swing.JLabel>();
        jTextArea4=new ArrayList<javax.swing.JTextArea>();
        
        
        for (int i =0; i<cm.size();i++)
        {
            javax.swing.JScrollPane jScrollPane9= new JScrollPane();
            javax.swing.JPanel jPanel26= new JPanel();
            javax.swing.JLabel jLabel40=new JLabel();
            javax.swing.JLabel jLabel41=new JLabel();
            javax.swing.JTextArea jTextArea5=new JTextArea();
            jScrollPane8.add(jScrollPane9);
            jPanel25.add(jPanel26);
            jLabel38.add(jLabel40);
            jLabel39.add(jLabel41);
            jTextArea4.add(jTextArea5);
        jPanel25.get(i).setBackground(new java.awt.Color(255, 255, 255));
        jPanel25.get(i).setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 0), 2));
        jPanel25.get(i).setMinimumSize(new java.awt.Dimension(422, 108));
        jPanel25.get(i).setPreferredSize(new java.awt.Dimension(490, 102));
        jPanel25.get(i).setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        PlaceModel place = PlaceModel.getPlace(cm.get(i).ID);
        jLabel38.get(i).setText(cm.get(i).userName+" Checked in "+place.Place_Name);
        jPanel25.get(i).add(jLabel38.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 200, -1));

        jTextArea4.get(i).setColumns(20);
        jTextArea4.get(i).setRows(5);
        jTextArea4.get(i).setText(cm.get(i).text);
        jScrollPane8.get(i).setViewportView(jTextArea4.get(i));

        jPanel25.get(i).add(jScrollPane8.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 360, 69));

        jLabel39.get(i).setText(String.valueOf(cm.get(i).LikesNum));
        jLabel39.get(i).setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Like-Button.png")));
        jPanel25.get(i).add(jLabel39.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 50, -1, -1));

        jPanel14.add(jPanel25.get(i), new org.netbeans.lib.awtextra.AbsoluteConstraints(0, i*125, 490, 120));
        }
   }
   
    
    private void Show ()
    {
        Name.setText(User.getUser().getUserName());
        Type.setText(User.getUser().getUser_Type());
        Phone.setText(User.getUser().getPhone_Number());
        Email.setText(User.getUser().getEmail());
        Gender.setText(User.getUser().getGender());
        Address.setText(User.getUser().getAddress());
    }
    private Main_Menu() {
        Name.setText("");
        Type.setText("");
        Phone.setText("");
        Email.setText("");
        Gender.setText("");
        Address.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane6 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        newpass = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        confirmPass = new javax.swing.JLabel();
        oldPass = new javax.swing.JLabel();
        newPassText = new javax.swing.JPasswordField();
        confirmPassText = new javax.swing.JPasswordField();
        OldPasstext = new javax.swing.JTextField();
        Submit = new javax.swing.JButton();
        Cancel = new javax.swing.JButton();
        Type = new javax.swing.JTextField();
        Phone = new javax.swing.JTextField();
        Email = new javax.swing.JTextField();
        Gender = new javax.swing.JTextField();
        Address = new javax.swing.JTextField();
        Name = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        FrinList = new javax.swing.JList();
        jLabel49 = new javax.swing.JLabel();
        Name2 = new javax.swing.JTextField();
        Type2 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        Phone2 = new javax.swing.JTextField();
        jLabel53 = new javax.swing.JLabel();
        Email2 = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        Gender2 = new javax.swing.JTextField();
        jLabel55 = new javax.swing.JLabel();
        Address3 = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jLabel50 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        Name1 = new javax.swing.JTextField();
        Type1 = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        Phone1 = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        Email1 = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        Address1 = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        FrinList2 = new javax.swing.JList();
        Gender1 = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jLabel48 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jTabbedPane7 = new javax.swing.JTabbedPane();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel15 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextArea5 = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        jScrollPane10 = new javax.swing.JScrollPane();
        jPanel16 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jLabel41 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        FavList = new javax.swing.JList();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        Rate2 = new javax.swing.JTextField();
        saved2 = new javax.swing.JTextField();
        check2 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        Desc2 = new javax.swing.JTextArea();
        CheckIn1 = new javax.swing.JButton();
        Address2 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        AllList = new javax.swing.JList();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        Rate = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        saved = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        check = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        Desc = new javax.swing.JTextArea();
        jLabel33 = new javax.swing.JLabel();
        Save1 = new javax.swing.JButton();
        CheckIn2 = new javax.swing.JButton();
        jLabel34 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel14 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setText("Phone Number : ");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 170, -1, -1));

        jLabel4.setText("Email : ");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, -1));

        jLabel5.setText("Gender : ");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 250, -1, -1));

        newpass.setVisible(false);
        newpass.setText("New Password :");
        jPanel2.add(newpass, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 400, -1, -1));

        jLabel7.setText("Profile Photo ");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 30, -1, -1));

        jLabel8.setText("Name : ");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        jLabel13.setText("___________________________________________________________________");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 310, 420, 20));

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/765-default-avatar22222.png"))); // NOI18N
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 40, -1, 160));

        jLabel16.setText("User Type : ");
        jPanel2.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));

        jLabel17.setForeground(new java.awt.Color(0, 0, 255));
        jLabel17.setText("2- Update User Information .");
        jLabel17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel17MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 340, -1, -1));

        jLabel19.setForeground(new java.awt.Color(0, 0, 255));
        jLabel19.setText("1- Change Password .");
        jLabel19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel19MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 340, -1, -1));

        jLabel20.setText("Address :");
        jPanel2.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, -1, -1));

        confirmPass.setVisible(false);
        confirmPass.setText("Confirm Password :");
        jPanel2.add(confirmPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 430, -1, -1));

        oldPass.setVisible(false);
        oldPass.setText("Old Password :");
        jPanel2.add(oldPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 370, -1, -1));

        newPassText.setVisible(false);
        jPanel2.add(newPassText, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 400, 200, -1));

        confirmPassText.setVisible(false);
        confirmPassText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmPassTextActionPerformed(evt);
            }
        });
        jPanel2.add(confirmPassText, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 430, 200, -1));

        OldPasstext.setVisible(false);
        jPanel2.add(OldPasstext, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 370, 200, 30));

        Submit.setVisible(false);
        Submit.setText("Submit");
        Submit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubmitActionPerformed(evt);
            }
        });
        jPanel2.add(Submit, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 380, 70, -1));

        Cancel.setVisible(false);
        Cancel.setText("Cancel");
        Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelActionPerformed(evt);
            }
        });
        jPanel2.add(Cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 420, 70, -1));

        Type.setEditable(false);
        Type.setText("User Type");
        Type.setBorder(null);
        jPanel2.add(Type, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 130, 150, 20));

        Phone.setEditable(false);
        Phone.setText("User Phone Number");
        Phone.setBorder(null);
        jPanel2.add(Phone, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 170, 150, 20));

        Email.setEditable(false);
        Email.setText("User_Email@Allways.com");
        Email.setBorder(null);
        jPanel2.add(Email, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 210, 210, 20));

        Gender.setEditable(false);
        Gender.setText("Male/Female");
        Gender.setBorder(null);
        jPanel2.add(Gender, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 250, 150, 20));

        Address.setEditable(false);
        Address.setText("User Address");
        Address.setBorder(null);
        jPanel2.add(Address, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 290, 150, 20));

        Name.setEditable(false);
        Name.setText("User Name");
        Name.setBorder(null);
        Name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NameActionPerformed(evt);
            }
        });
        jPanel2.add(Name, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 90, 150, 20));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/gps_location_map_marker-512888.png"))); // NOI18N
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, -1, 480));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/gps_location_map_marker-5131.png"))); // NOI18N
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 500, 500));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/gps_location_map_marker-5128.png"))); // NOI18N
        jLabel6.setText("jLabel6");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, 490, 30));

        jTabbedPane6.addTab("Profile", jPanel2);

        jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        FrinList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        FrinList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FrinListMouseClicked(evt);
            }
        });
        jScrollPane12.setViewportView(FrinList);

        jPanel11.add(jScrollPane12, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, 420, 80));

        jLabel49.setText("Name : ");
        jPanel11.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, -1, -1));

        Name2.setEditable(false);
        Name2.setText("User Name");
        Name2.setBorder(null);
        Name2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Name2ActionPerformed(evt);
            }
        });
        jPanel11.add(Name2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 190, 150, 20));

        Type2.setEditable(false);
        Type2.setText("User Type");
        Type2.setBorder(null);
        jPanel11.add(Type2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 230, 150, 20));

        jLabel51.setText("User Type : ");
        jPanel11.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 230, -1, -1));

        jLabel52.setText("Phone Number : ");
        jPanel11.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 270, -1, -1));

        Phone2.setEditable(false);
        Phone2.setText("User Phone Number");
        Phone2.setBorder(null);
        jPanel11.add(Phone2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 270, 150, 20));

        jLabel53.setText("Email : ");
        jPanel11.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 310, -1, -1));

        Email2.setEditable(false);
        Email2.setText("User_Email@Allways.com");
        Email2.setBorder(null);
        jPanel11.add(Email2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 310, 190, 20));

        jLabel54.setText("Gender : ");
        jPanel11.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 350, -1, -1));

        Gender2.setEditable(false);
        Gender2.setText("Male/Female");
        Gender2.setBorder(null);
        jPanel11.add(Gender2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 350, 150, 20));

        jLabel55.setText("Address :");
        jPanel11.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 390, -1, -1));

        Address3.setEditable(false);
        Address3.setText("User Address");
        Address3.setBorder(null);
        jPanel11.add(Address3, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 390, 150, 20));

        jLabel56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/765-default-avatar22222.png"))); // NOI18N
        jPanel11.add(jLabel56, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 150, -1, 160));

        jLabel57.setText("Profile Photo ");
        jPanel11.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 130, -1, -1));

        jButton6.setBackground(new java.awt.Color(255, 255, 255));
        jButton6.setText("Delete Friend");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel11.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 380, -1, -1));

        jLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Wallpaper.jpg"))); // NOI18N
        jPanel11.add(jLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 500));

        jTabbedPane2.addTab("My Fiends", jPanel11);

        jPanel12.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel35.setText("Name : ");
        jPanel12.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, -1, -1));

        Name1.setEditable(false);
        Name1.setText("User Name");
        Name1.setBorder(null);
        Name1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Name1ActionPerformed(evt);
            }
        });
        jPanel12.add(Name1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 190, 150, 20));

        Type1.setEditable(false);
        Type1.setText("User Type");
        Type1.setBorder(null);
        jPanel12.add(Type1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 230, 150, 20));

        jLabel42.setText("User Type : ");
        jPanel12.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 230, -1, -1));

        jLabel36.setText("Phone Number : ");
        jPanel12.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 270, -1, -1));

        Phone1.setEditable(false);
        Phone1.setText("User Phone Number");
        Phone1.setBorder(null);
        jPanel12.add(Phone1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 270, 150, 20));

        jLabel43.setText("Email : ");
        jPanel12.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 310, -1, -1));

        Email1.setEditable(false);
        Email1.setText("User_Email@Allways.com");
        Email1.setBorder(null);
        jPanel12.add(Email1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 310, 190, 20));

        jLabel44.setText("Gender : ");
        jPanel12.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 350, -1, -1));

        jLabel45.setText("Address :");
        jPanel12.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 390, -1, -1));

        Address1.setEditable(false);
        Address1.setText("User Address");
        Address1.setBorder(null);
        jPanel12.add(Address1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 390, 150, 20));

        jLabel46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/765-default-avatar22222.png"))); // NOI18N
        jPanel12.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 150, -1, 160));

        jLabel47.setText("Profile Photo ");
        jPanel12.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 130, -1, -1));

        FrinList2.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        FrinList2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FrinList2MouseClicked(evt);
            }
        });
        jScrollPane11.setViewportView(FrinList2);

        jPanel12.add(jScrollPane11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, 420, 80));

        Gender1.setEditable(false);
        Gender1.setText("Male/Female");
        Gender1.setBorder(null);
        jPanel12.add(Gender1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 350, 150, 20));

        jButton5.setBackground(new java.awt.Color(255, 255, 255));
        jButton5.setText("Add");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel12.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 400, 70, -1));

        jLabel48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Wallpaper.jpg"))); // NOI18N
        jPanel12.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 500));

        jTabbedPane2.addTab("New Friends", jPanel12);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        jTabbedPane6.addTab("Friends", jPanel3);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 490, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 502, Short.MAX_VALUE)
        );

        jTabbedPane7.addTab("Messages", jPanel7);

        jPanel15.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel21.setBackground(new java.awt.Color(255, 255, 255));
        jPanel21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 0), 2));
        jPanel21.setMinimumSize(new java.awt.Dimension(422, 108));
        jPanel21.setPreferredSize(new java.awt.Dimension(490, 102));
        jPanel21.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel40.setText("Check in title :");
        jPanel21.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 200, -1));

        jTextArea5.setColumns(20);
        jTextArea5.setRows(5);
        jScrollPane9.setViewportView(jTextArea5);

        jPanel21.add(jScrollPane9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 360, 69));

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Like-Button.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonX_Live_ActionPerformed(evt);
            }
        });
        jPanel21.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 40, 50, 50));

        jPanel15.add(jPanel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 490, 120));

        jScrollPane6.setViewportView(jPanel15);

        jTabbedPane7.addTab("Live Notifications", jScrollPane6);

        jPanel16.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));
        jPanel22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 0), 2));
        jPanel22.setMinimumSize(new java.awt.Dimension(422, 108));
        jPanel22.setPreferredSize(new java.awt.Dimension(490, 102));
        jPanel22.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel41.setText("Check in title :");
        jPanel22.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 100, -1));

        jButton3.setBackground(new java.awt.Color(255, 255, 255));
        jButton3.setText("Discard");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonY_Friend_ActionPerformed(evt);
            }
        });
        jPanel22.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 30, 100, 30));

        jButton4.setBackground(new java.awt.Color(255, 255, 255));
        jButton4.setText("Accept");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonX_Friend_ActionPerformed(evt);
            }
        });
        jPanel22.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 30, 90, 30));

        jPanel16.add(jPanel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 490, 80));

        jScrollPane10.setViewportView(jPanel16);

        jTabbedPane7.addTab("Freinds Request", jScrollPane10);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane7)
        );

        jTabbedPane6.addTab("Notfications", jPanel4);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 495, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 530, Short.MAX_VALUE)
        );

        jTabbedPane6.addTab("Tastes", jPanel5);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        FavList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FavListMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(FavList);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, 410, 70));

        jLabel10.setText("Saves :");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 350, -1, -1));

        jLabel11.setText("Map :");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 110, -1, -1));

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/map1.jpg"))); // NOI18N
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 130, -1, -1));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/View1.png"))); // NOI18N
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 130, -1, -1));

        jTextField1.setEditable(false);
        jTextField1.setText("_______________________________________________________________________");
        jTextField1.setBorder(null);
        jPanel1.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, 420, -1));

        jLabel18.setText("Picture : ");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 110, -1, -1));

        jLabel21.setText("Address : ");
        jPanel1.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, -1, -1));

        jLabel22.setText("Checks in : ");
        jPanel1.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 350, -1, -1));

        jLabel23.setText("Description :");
        jPanel1.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, -1, -1));

        jLabel24.setText("Rate : ");
        jPanel1.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 350, -1, -1));
        jPanel1.add(Rate2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 350, 80, 20));
        jPanel1.add(saved2, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 350, 90, 20));
        jPanel1.add(check2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 350, 60, -1));

        Desc2.setColumns(20);
        Desc2.setRows(5);
        jScrollPane2.setViewportView(Desc2);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 380, 320, 50));

        CheckIn1.setBackground(new java.awt.Color(255, 153, 0));
        CheckIn1.setText("Check In");
        CheckIn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckIn1ActionPerformed(evt);
            }
        });
        jPanel1.add(CheckIn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 440, 190, -1));
        jPanel1.add(Address2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 320, 300, 20));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Wallpaper.jpg"))); // NOI18N
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jTabbedPane1.addTab("Favorit Places", jPanel1);

        jPanel10.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        AllList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AllListMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(AllList);

        jPanel10.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, 410, 70));

        jLabel25.setText("Map :");
        jPanel10.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 110, -1, -1));

        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/map1.jpg"))); // NOI18N
        jPanel10.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 130, -1, -1));

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/View1.png"))); // NOI18N
        jPanel10.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 130, -1, -1));

        jLabel28.setText("Picture : ");
        jPanel10.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 110, -1, -1));

        jTextField6.setEditable(false);
        jTextField6.setText("_______________________________________________________________________");
        jTextField6.setBorder(null);
        jPanel10.add(jTextField6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 290, 420, -1));
        jPanel10.add(jTextField7, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 320, 300, 20));

        jLabel29.setText("Address : ");
        jPanel10.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, -1, -1));

        jLabel30.setText("Rate : ");
        jPanel10.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 350, -1, -1));
        jPanel10.add(Rate, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 350, 80, 20));

        jLabel31.setText("Saves :");
        jPanel10.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 350, -1, -1));
        jPanel10.add(saved, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 350, 90, 20));

        jLabel32.setText("Checks in : ");
        jPanel10.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 350, -1, -1));
        jPanel10.add(check, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 350, 60, -1));

        Desc.setColumns(20);
        Desc.setRows(5);
        jScrollPane4.setViewportView(Desc);

        jPanel10.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 380, 320, 50));

        jLabel33.setText("Description :");
        jPanel10.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, -1, -1));

        Save1.setBackground(new java.awt.Color(255, 153, 0));
        Save1.setText("Save to Favorit");
        Save1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Save1ActionPerformed(evt);
            }
        });
        jPanel10.add(Save1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 440, 190, -1));

        CheckIn2.setBackground(new java.awt.Color(255, 153, 0));
        CheckIn2.setText("Check In");
        CheckIn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckIn2ActionPerformed(evt);
            }
        });
        jPanel10.add(CheckIn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 440, 190, -1));

        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Wallpaper.jpg"))); // NOI18N
        jPanel10.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jTabbedPane1.addTab("All Places", jPanel10);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane6.addTab("Places", jPanel6);

        jPanel14.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jScrollPane5.setViewportView(jPanel14);

        jTabbedPane6.addTab("Checks In", jScrollPane5);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane6)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void NameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NameActionPerformed

    private void CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelActionPerformed
        oldPass.setVisible(false);
        OldPasstext.setVisible(false);
        newPassText.setVisible(false);
        newpass.setVisible(false);
        confirmPass.setVisible(false);
        confirmPassText.setVisible(false);
        Submit.setVisible(false);
        Cancel.setVisible(false);
        visablity1=false;
        visablity2=false;
        Show();
    }//GEN-LAST:event_CancelActionPerformed

    private void SubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubmitActionPerformed

        CancelActionPerformed(evt);
    }//GEN-LAST:event_SubmitActionPerformed

    private void jLabel19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseClicked
        if (visablity1==false&&visablity2==false)
        {newpass.setText("New Password: ");
            oldPass.setVisible(true);
            OldPasstext.setVisible(true);
            newPassText.setVisible(true);
            newpass.setVisible(true);
            confirmPass.setVisible(true);
            confirmPassText.setVisible(true);
            Submit.setVisible(true);
            Cancel.setVisible(true);
            visablity1=true;
        }

    }//GEN-LAST:event_jLabel19MouseClicked

    private void jLabel17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MouseClicked
        if (visablity2==false&&visablity1==false)
        {
            newpass.setText("Password: ");
            newPassText.setVisible(true);
            newpass.setVisible(true);
            Submit.setVisible(true);
            Cancel.setVisible(true);
            visablity2=true;
        }

    }//GEN-LAST:event_jLabel17MouseClicked

    private void CheckIn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckIn1ActionPerformed

        int i=FavList.getSelectedIndex();
        String Text=JOptionPane.showInputDialog("Write Your CheckIn");
        CheckInModel c=new CheckInModel(PP.get(i).Place_ID,Text,User.getUser().UserName);
        try {
            c.saveCheckIn();
        } catch (SQLException ex) {
            Logger.getLogger(Main_Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_CheckIn1ActionPerformed

    private void Save1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Save1ActionPerformed
        // TODO add your handling code here:
        int i=AllList.getSelectedIndex();
        PP.get(i).SavePlace(User.getUser().UserName);
    }//GEN-LAST:event_Save1ActionPerformed

    private void CheckIn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckIn2ActionPerformed
        // TODO add your handling code here:
        int i=AllList.getSelectedIndex();
        String Text=JOptionPane.showInputDialog("Write Your CheckIn");
        CheckInModel c=new CheckInModel(PP.get(i).Place_ID,Text,User.getUser().UserName);
        try {
            c.saveCheckIn();
        } catch (SQLException ex) {
            Logger.getLogger(Main_Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_CheckIn2ActionPerformed

    private void confirmPassTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmPassTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_confirmPassTextActionPerformed

    private void FavListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FavListMouseClicked
        String Selected = FavList.getSelectedValue().toString();
        int i=FavList.getSelectedIndex();
        Address2.setText("");
        Rate2.setText(String.valueOf(PP.get(i).Rate));
        saved2.setText(String.valueOf(PP.get(i).SavedNum));
        check2.setText(String.valueOf(PP.get(i).CheckInSum));
        Desc2.setText(String.valueOf(PP.get(i).Place_Description));
    }//GEN-LAST:event_FavListMouseClicked

    private void AllListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AllListMouseClicked
        String Selected = AllList.getSelectedValue().toString();
        int i=AllList.getSelectedIndex();
        Address.setText("");
        Rate.setText(String.valueOf(PP.get(i).Rate));
        saved.setText(String.valueOf(PP.get(i).SavedNum));
        check.setText(String.valueOf(PP.get(i).CheckInSum));
        Desc.setText(String.valueOf(PP.get(i).Place_Description));                        
    }//GEN-LAST:event_AllListMouseClicked

    private void Name2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Name2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Name2ActionPerformed

    private void Name1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Name1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Name1ActionPerformed

    private void jButtonX_Live_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonX_Live_ActionPerformed

    }//GEN-LAST:event_jButtonX_Live_ActionPerformed

    private void jButtonY_Friend_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonY_Friend_ActionPerformed
        int j =0;
        for( ;j<10;j++)
        {
            if(JButtony.get(j).isFocusOwner())
                break;
        }
    }//GEN-LAST:event_jButtonY_Friend_ActionPerformed

    private void jButtonX_Friend_ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonX_Friend_ActionPerformed
        
    }//GEN-LAST:event_jButtonX_Friend_ActionPerformed

    private void FrinListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FrinListMouseClicked
        // TODO add your handling code here:
        String Selected = FrinList.getSelectedValue().toString();
        int i=FrinList.getSelectedIndex();
        Name2.setText(Friend.get(i).getUserName());
        Type2.setText(Friend.get(i).getUser_Type());
        Phone2.setText(Friend.get(i).getPhone_Number());
        Email2.setText(Friend.get(i).getEmail());
        Gender2.setText(Friend.get(i).getGender());
        Address2.setText(Friend.get(i).getAddress());
    }//GEN-LAST:event_FrinListMouseClicked

    private void FrinList2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FrinList2MouseClicked
        // TODO add your handling code here:
        String Selected = FrinList2.getSelectedValue().toString();
        int i=FrinList2.getSelectedIndex();
        Name1.setText(newFriend.get(i).getUserName());
        Type1.setText(newFriend.get(i).getUser_Type());
        Phone1.setText(newFriend.get(i).getPhone_Number());
        Email1.setText(newFriend.get(i).getEmail());
        Gender1.setText(newFriend.get(i).getGender());
        Address1.setText(newFriend.get(i).getAddress());
    }//GEN-LAST:event_FrinList2MouseClicked

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        try {
            // TODO add your handling code here:
            int i=FrinList2.getSelectedIndex();
            User.getUser().addNewFreind(newFriend.get(i).getUserName());
        } catch (SQLException ex) {
            Logger.getLogger(Main_Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        try {
            // TODO add your handling code here:
            int i=FrinList.getSelectedIndex();
            User.getUser().deleteUser(Friend.get(i).getUserName());
        } catch (SQLException ex) {
            Logger.getLogger(Main_Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main_Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Address;
    private javax.swing.JTextField Address1;
    private javax.swing.JTextField Address2;
    private javax.swing.JTextField Address3;
    private javax.swing.JList AllList;
    private javax.swing.JButton Cancel;
    private javax.swing.JButton CheckIn1;
    private javax.swing.JButton CheckIn2;
    private javax.swing.JTextArea Desc;
    private javax.swing.JTextArea Desc2;
    private javax.swing.JTextField Email;
    private javax.swing.JTextField Email1;
    private javax.swing.JTextField Email2;
    private javax.swing.JList FavList;
    private javax.swing.JList FrinList;
    private javax.swing.JList FrinList2;
    private javax.swing.JTextField Gender;
    private javax.swing.JTextField Gender1;
    private javax.swing.JTextField Gender2;
    private javax.swing.JTextField Name;
    private javax.swing.JTextField Name1;
    private javax.swing.JTextField Name2;
    private javax.swing.JTextField OldPasstext;
    private javax.swing.JTextField Phone;
    private javax.swing.JTextField Phone1;
    private javax.swing.JTextField Phone2;
    private javax.swing.JTextField Rate;
    private javax.swing.JTextField Rate2;
    private javax.swing.JButton Save1;
    private javax.swing.JButton Submit;
    private javax.swing.JTextField Type;
    private javax.swing.JTextField Type1;
    private javax.swing.JTextField Type2;
    private javax.swing.JTextField check;
    private javax.swing.JTextField check2;
    private javax.swing.JLabel confirmPass;
    private javax.swing.JPasswordField confirmPassText;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane6;
    private javax.swing.JTabbedPane jTabbedPane7;
    private javax.swing.JTextArea jTextArea5;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JPasswordField newPassText;
    private javax.swing.JLabel newpass;
    private javax.swing.JLabel oldPass;
    private javax.swing.JTextField saved;
    private javax.swing.JTextField saved2;
    // End of variables declaration//GEN-END:variables
}
