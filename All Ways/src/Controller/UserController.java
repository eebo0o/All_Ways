package Controller;

import Model.UserModel;
import java.sql.*;
import java.util.*;


public class UserController {

    private UserModel User ;

    public void setUser(UserModel U) {
        User = U;
    }

    public UserModel getUser() {
        return User;
    }
    
    public UserController() {
        User=new UserModel();
    }
    
    public UserModel login(String name , String Password) throws SQLException{
        User=UserModel.getUser(name, Password);
        return User;
        
    }
    static public void signUp(UserModel User){
        User.addNewUser(User);
    }
    public UserModel forgetPassword(String name,String Mail,String Phone) throws SQLException{
        UserModel U = UserModel.getUserByPassword(name,Mail, Phone);
        if(U != null){
            User = U;
            return User;
        }
        return null ;
    }
    public void sendRequest(){
        
    }
    public void acceptRequest() throws SQLException{
        User.addNewFreind(User.getUserName());
    }
    
    
}
